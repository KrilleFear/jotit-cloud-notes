import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:jotit_cloud_notes/model/nextcloud_credentials.dart';
import 'package:jotit_cloud_notes/model/nextcloud_notes_api.dart';
import 'package:jotit_cloud_notes/model/note.dart';
import 'package:jotit_cloud_notes/model/store.dart';
import 'package:provider/provider.dart';

class JotitCloudNotes {
  JotitCloudNotes() {
    _ready = _load();
  }
  factory JotitCloudNotes.of(BuildContext context) =>
      Provider.of<JotitCloudNotes>(context);

  final StreamController<List<String>> onUpdate = StreamController.broadcast();

  Future<void> _ready;
  Future<void> get ready => _ready;

  NextcloudCredentials get nextcloudCredentials => _api?.nextcloudCredentials;

  bool get cloudMode => nextcloudCredentials != null;

  NextcloudNotesApi _api;

  List<Note> _notes = [];
  List<Note> get notes => _notes;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextcloud_credentials'] = nextcloudCredentials?.toJson();
    data['notes'] = notes.map((n) => n.toJson()).toList();
    return data;
  }

  void _loadFromJson(Map<String, dynamic> data) {
    if (data['nextcloud_credentials'] != null) {
      _api = NextcloudNotesApi(
          NextcloudCredentials.fromJson(data['nextcloud_credentials']));
    }
    _notes =
        (data['notes'] as List).map((json) => Note.fromJson(json)).toList();
  }

  Future<void> _load() async {
    _loadFromJson(await Store().load());
    return;
  }

  Future<void> _save() async => Store().save(toJson());

  void _sortNotes() => _notes.sort(
        (a, b) => (a.favorite != b.favorite)
            ? (a.favorite ? -1 : 1)
            : b.modified.compareTo(a.modified),
      );

  Future<Note> createNote(String content, {bool upload = true}) async {
    var note = Note(content: content);
    notes.add(note);
    _sortNotes();
    await _save();
    onUpdate.add([note.id]);
    if (_api != null) {
      try {
        final cloudNote = await _api.createNote(
          note.content,
          category: note.category,
          modified: (note.modified.millisecondsSinceEpoch / 1000).round(),
          favorite: note.favorite,
        );
        note
          ..cloudId = cloudNote.cloudId
          ..modified = cloudNote.modified
          ..category = cloudNote.category
          ..title = cloudNote.title
          ..favorite = cloudNote.favorite
          ..synchronized = true;
        await _save();
        onUpdate.add([note.id]);
      } catch (e) {
        debugPrint(e.toString());
      }
    }
    return note;
  }

  Future<void> deleteNote(String id) async {
    final cloudId = notes.singleWhere((n) => n.id == id).cloudId;
    notes.removeWhere((n) => n.id == id);
    await _save();
    onUpdate.add([]);
    await _api?.deleteNote(cloudId);
    return;
  }

  Future<Note> updateNote(
    String id, {
    String title,
    String content,
    String category,
    bool favorite,
  }) async {
    final note = notes.singleWhere((n) => n.id == id);
    note.modified = DateTime.now();
    if (title != null) note.title = title;
    if (content != null) note.content = content;
    if (category != null) note.category = category;
    if (favorite != null) note.favorite = favorite;
    _sortNotes();
    await _save();
    onUpdate.add([note.id]);
    note.synchronized = false;
    if (_api != null) {
      await _api.updateNote(
        note.cloudId,
        note.content,
        category: note.category,
        modified: (note.modified.millisecondsSinceEpoch / 1000).round(),
        favorite: note.favorite,
      );
      note.synchronized = true;
      await _save();
      onUpdate.add([note.id]);
    }
    return note;
  }

  Future<void> setNextcloudCredentials(
    String username,
    String password,
    String url,
  ) async {
    _api = NextcloudNotesApi(
      NextcloudCredentials(
        username: username,
        password: password,
        url: url,
      ),
    );
    try {
      await cloudSync();
      await _save();
    } catch (_) {
      _api = null;
      rethrow;
    }
  }

  void deleteNextcloudCredentials() async {
    _api = null;
    await _save();
  }

  Future<void> cloudSync() async {
    final cloudNotes = await _api.requestAllNotes();

    for (var note in notes) {
      note.synchronized = false;
    }

    final updatedNoteIds = <String>[];
    for (var cloudNote in cloudNotes) {
      final localIndex = notes
          .indexWhere((localNote) => localNote.cloudId == cloudNote.cloudId);
      if (localIndex == -1) {
        notes.add(cloudNote..synchronized = true);
      } else {
        final localNote = notes[localIndex];
        if (localNote.modified == cloudNote.modified &&
            localNote.content == cloudNote.content &&
            localNote.title == cloudNote.title &&
            localNote.favorite == cloudNote.favorite &&
            localNote.category == cloudNote.category) {
          debugPrint('Already synchronized!');
          localNote.synchronized = true;
          updatedNoteIds.add(localNote.id);
        } else if (localNote.modified.millisecondsSinceEpoch >
            cloudNote.modified.millisecondsSinceEpoch) {
          debugPrint('Local note ${cloudNote.id} is newer - Upload it...');
          await _api.updateNote(
            cloudNote.cloudId,
            localNote.content,
            category: localNote.category,
            modified:
                (localNote.modified.millisecondsSinceEpoch / 1000).round(),
            favorite: localNote.favorite,
          );
          localNote.synchronized = true;
          updatedNoteIds.add(localNote.id);
        } else {
          debugPrint('Cloud note ${cloudNote.id} is newer - Download it...');
          notes[localIndex] = cloudNote..synchronized = true;
          updatedNoteIds.add(cloudNote.id);
        }
      }
    }

    for (var note in notes.where((n) => !n.synchronized)) {
      debugPrint('Upload local note from ${note.modified.toIso8601String()}');
      final cloudNote = await _api.createNote(
        note.content,
        category: note.category,
        modified: (note.modified.millisecondsSinceEpoch / 1000).round(),
        favorite: note.favorite,
      );
      note
        ..synchronized = true
        ..cloudId = cloudNote.cloudId
        ..title = cloudNote.title;
    }

    _sortNotes();
    await _save();
    onUpdate.add(updatedNoteIds);
  }
}
