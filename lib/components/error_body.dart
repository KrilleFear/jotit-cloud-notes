import 'package:flutter/material.dart';

class ErrorBody extends StatelessWidget {
  final Object exception;

  const ErrorBody({Key key, @required this.exception}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.error),
          Text('Oops something went wrong...'),
          Text(exception.toString()),
        ],
      ),
    );
  }
}
