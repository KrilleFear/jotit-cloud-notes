# Privacy Policy

Jotit Cloud Notes is a simple note taking app which **can** use the Nextcloud Notes API to store notes on a Nextcloud instance which the user needs to choose first. The app can work fully offline too. The app will only communicate with the chosen Nextcloud instance.

More informations about Nextcloud Notes: https://github.com/nextcloud/notes

Notes are cached on the user's device in an encrypted store using the Fluter Secure Storage.

More informations about Flutter Secure Storage: https://pub.dev/packages/flutter_secure_storage