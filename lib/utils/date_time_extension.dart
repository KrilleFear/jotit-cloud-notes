import 'package:flutter/material.dart';

extension DateTimeExtension on DateTime {
  String getLocalizedDay(BuildContext context) {
    return '${day.toString().padLeft(2, '0')}.${month.toString().padLeft(2, '0')}.$year';
  }

  String getLocalizedDayTime(BuildContext context) {
    return '${getLocalizedDay(context)} - ${hour.toString().padLeft(2, '0')}:${minute.toString().padLeft(2, '0')}';
  }

  bool sameDay(DateTime other) {
    return day == other.day && month == other.month && year == other.year;
  }
}
