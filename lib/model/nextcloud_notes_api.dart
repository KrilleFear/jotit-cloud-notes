import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:jotit_cloud_notes/model/nextcloud_credentials.dart';
import 'package:jotit_cloud_notes/model/note.dart';

class NextcloudNotesApi {
  final NextcloudCredentials nextcloudCredentials;

  final http.Client _client;

  NextcloudNotesApi(this.nextcloudCredentials, {http.Client client})
      : _client = client ?? http.Client();

  String get _notesApiUrl =>
      'https://${nextcloudCredentials.username}:${nextcloudCredentials.password}@${nextcloudCredentials.url}/index.php/apps/notes/api/v0.2/notes';
  String _notesApiUrlWithId(int id) => '$_notesApiUrl/$id';

  Future<List<Note>> requestAllNotes() async {
    final response = await _client.get(_notesApiUrl);
    return (response.jsonBody['chunk'] as List)
        .map((json) => Note.fromJson(json))
        .toList();
  }

  Future<Note> requestNote(int id) async {
    final response = await _client.get(_notesApiUrlWithId(id));
    return Note.fromJson(response.jsonBody);
  }

  Future<Note> createNote(
    String content, {
    String category,
    int modified,
    bool favorite,
  }) async {
    final response = await _client.post(
      _notesApiUrl,
      body: jsonEncode(
        {
          'content': content,
          if (category != null) 'category': category,
          if (modified != null) 'modified': modified,
          if (favorite != null) 'favorite': favorite,
        },
      ),
      headers: {'Content-Type': 'application/json'},
    );
    return Note.fromJson(response.jsonBody);
  }

  Future<Note> updateNote(
    int id,
    String content, {
    String category,
    int modified,
    bool favorite,
  }) async {
    final response = await _client.put(
      _notesApiUrlWithId(id),
      body: jsonEncode(
        {
          'content': content,
          if (category != null && category.isNotEmpty) 'category': category,
          if (modified != null) 'modified': modified,
          if (favorite != null) 'favorite': favorite,
        },
      ),
      headers: {'Content-Type': 'application/json'},
    );
    return Note.fromJson(response.jsonBody);
  }

  Future<void> deleteNote(int id) async {
    await _client.delete(
      _notesApiUrlWithId(id),
    );
    return;
  }
}

extension on http.Response {
  Map<String, dynamic> get jsonBody {
    if (statusCode < 200 || statusCode >= 300) throw Exception(reasonPhrase);
    var respBody = body;
    try {
      respBody = utf8.decode(bodyBytes);
    } catch (_) {}
    var jsonString = String.fromCharCodes(respBody.runes);
    if (jsonString.startsWith('[') && jsonString.endsWith(']')) {
      jsonString = '\{"chunk":$jsonString\}';
    }
    return jsonDecode(jsonString) as Map<String, dynamic>;
  }
}
