import 'package:flushbar/flushbar_helper.dart';
import 'package:flutter/material.dart';
import 'package:jotit_cloud_notes/model/jotit_cloud_notes.dart';

class CloudSettingsView extends StatefulWidget {
  @override
  _CloudSettingsViewState createState() => _CloudSettingsViewState();
}

class _CloudSettingsViewState extends State<CloudSettingsView> {
  final TextEditingController _domainController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  String _domainError, _usernameError, _passwordError;

  JotitCloudNotes jotit;

  bool _isLoading = false;

  void _connectAction() async {
    setState(() {
      _isLoading = true;
      _domainError = _usernameError = _passwordError = null;
    });

    if (_domainController.text.isEmpty) {
      return setState(() {
        _domainError = 'Please fill out';
        _isLoading = false;
      });
    }

    if (_usernameController.text.isEmpty) {
      return setState(() {
        _usernameError = 'Please fill out';
        _isLoading = false;
      });
    }

    if (_passwordController.text.isEmpty) {
      return setState(() {
        _passwordError = 'Please fill out';
        _isLoading = false;
      });
    }

    try {
      await jotit.setNextcloudCredentials(
        _usernameController.text,
        _passwordController.text,
        _domainController.text,
      );
    } catch (e) {
      return setState(() {
        _passwordError = e.toString();
        _isLoading = false;
      });
    }
    setState(() {
      _isLoading = false;
    });
    await FlushbarHelper.createSuccess(
            message: 'Connection to the cloud has been established')
        .show(context);
    return;
  }

  void _deleteCredentials(JotitCloudNotes jotit) async {
    jotit.deleteNextcloudCredentials();
    _usernameController.clear();
    _passwordController.clear();
    _domainController.clear();
    setState(() => jotit = null);
  }

  @override
  Widget build(BuildContext context) {
    if (jotit == null) {
      jotit = JotitCloudNotes.of(context);
      if (jotit.cloudMode) {
        _domainController.text = jotit.nextcloudCredentials.url;
        _usernameController.text = jotit.nextcloudCredentials.username;
        _passwordController.text = jotit.nextcloudCredentials.password;
      }
    }
    return Scaffold(
      appBar: AppBar(
        title: Text('Set up your cloud'),
        actions: [
          if (jotit.cloudMode)
            IconButton(
              icon: Icon(Icons.delete_outline),
              onPressed: () => _deleteCredentials(jotit),
            ),
        ],
      ),
      body: ListView(
        padding: EdgeInsets.all(16),
        children: [
          TextField(
            readOnly: _isLoading,
            controller: _domainController,
            autofocus: true,
            autocorrect: false,
            keyboardType: TextInputType.url,
            decoration: InputDecoration(
              labelText: 'Domain',
              prefixText: 'https://',
              hintText: 'your_cloud_domain',
              border: OutlineInputBorder(),
              errorText: _domainError,
            ),
          ),
          SizedBox(height: 16),
          TextField(
            readOnly: _isLoading,
            controller: _usernameController,
            autocorrect: false,
            decoration: InputDecoration(
              labelText: 'Username',
              hintText: 'Username',
              border: OutlineInputBorder(),
              errorText: _usernameError,
            ),
          ),
          SizedBox(height: 16),
          TextField(
            readOnly: _isLoading,
            controller: _passwordController,
            autocorrect: false,
            obscureText: true,
            decoration: InputDecoration(
              labelText: 'Password',
              hintText: '******',
              border: OutlineInputBorder(),
              errorText: _passwordError,
            ),
          ),
          SizedBox(height: 16),
          RaisedButton(
            color: Theme.of(context).primaryColor,
            textColor: Colors.white,
            child: _isLoading ? LinearProgressIndicator() : Text('Connect'),
            onPressed: _isLoading ? null : _connectAction,
          ),
        ],
      ),
    );
  }
}
