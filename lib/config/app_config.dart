abstract class AppConfig {
  static const String name = 'Jotit Cloud Notes';
  static const String storeKey = 'jotit_cloud_notes_store';
}
