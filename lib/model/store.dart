import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jotit_cloud_notes/config/app_config.dart';
import 'package:localstorage/localstorage.dart';

class Store {
  static final Store _singleton = Store._internal();

  factory Store() {
    return _singleton;
  }

  Store._internal();

  final _secureStorage = !kIsWeb && (Platform.isAndroid || Platform.isIOS)
      ? FlutterSecureStorage()
      : null;
  final _localStorage = LocalStorage(AppConfig.storeKey);

  static const String _defaultValue = '{"notes":[]}';

  Future<Map<String, dynamic>> load() async {
    if (_secureStorage != null) {
      return jsonDecode((await _secureStorage.read(key: AppConfig.storeKey)) ??
          _defaultValue);
    }
    await _localStorage.ready;
    return jsonDecode(
        _localStorage.getItem(AppConfig.storeKey) ?? _defaultValue);
  }

  Future<void> save(Map<String, dynamic> json) async {
    if (_secureStorage != null) {
      return _secureStorage.write(
          key: AppConfig.storeKey, value: jsonEncode(json));
    }
    await _localStorage.ready;
    return _localStorage.setItem(AppConfig.storeKey, jsonEncode(json));
  }
}
