import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:floating_search_bar/floating_search_bar.dart';
import 'package:flushbar/flushbar_helper.dart';
import 'package:flutter/material.dart';
import 'package:jotit_cloud_notes/components/error_body.dart';
import 'package:jotit_cloud_notes/config/app_config.dart';
import 'package:jotit_cloud_notes/model/jotit_cloud_notes.dart';
import 'package:jotit_cloud_notes/views/cloud_settings_view.dart';
import 'package:jotit_cloud_notes/views/task_view.dart';
import 'package:url_launcher/url_launcher.dart';
import '../utils/date_time_extension.dart';

class TaskListView extends StatefulWidget {
  @override
  _TaskListViewState createState() => _TaskListViewState();
}

class _TaskListViewState extends State<TaskListView> {
  final Set<String> _selected = {};
  String _searchQuery = '';
  bool _isLoading = false;

  void _toggleSelect(String id) => _selected.contains(id)
      ? setState(() => _selected.remove(id))
      : setState(() => _selected.add(id));

  void _deleteSelected(JotitCloudNotes jotit) async {
    setState(() => _isLoading = true);
    for (var id in _selected) {
      await jotit.deleteNote(id).catchError(
            (e) =>
                FlushbarHelper.createError(message: e.toString()).show(context),
          );
    }
    _selected.clear();
    setState(() => _isLoading = false);
  }

  void _toggleFavorite(JotitCloudNotes jotit, String id) async {
    setState(() => _isLoading = true);
    try {
      await jotit.updateNote(id,
          favorite: !jotit.notes.singleWhere((n) => n.id == id).favorite);
    } catch (e) {
      // ignore: unawaited_futures
      FlushbarHelper.createError(message: e.toString()).show(context);
    }

    setState(() => _isLoading = false);
  }

  Future<void> _sync(JotitCloudNotes jotit) async {
    await jotit.ready;
    setState(() => _isLoading = true);
    try {
      await jotit.cloudSync();
    } catch (e) {
      // ignore: unawaited_futures
      FlushbarHelper.createError(message: e.toString()).show(context);
    }

    setState(() => _isLoading = false);
  }

  Future _firstSync;

  @override
  Widget build(BuildContext context) {
    final jotit = JotitCloudNotes.of(context);
    _firstSync ??= _sync(jotit);
    return FutureBuilder(
        future: jotit.ready,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return ErrorBody(exception: snapshot.error);
          }
          if (snapshot.connectionState != ConnectionState.done) {
            return Center(child: CircularProgressIndicator());
          }
          return StreamBuilder<Object>(
            stream: jotit.onUpdate.stream,
            builder: (context, snapshot) => Scaffold(
              floatingActionButton: _selected.isNotEmpty
                  ? null
                  : FloatingActionButton.extended(
                      icon: Icon(Icons.edit),
                      label: Text('Notiz hinzufügen'),
                      onPressed: () => Navigator.of(context)
                          .push(
                            MaterialPageRoute(
                              builder: (_) => TaskView(),
                            ),
                          )
                          .then((_) => _sync(jotit)),
                    ),
              body: SafeArea(
                child: Column(
                  children: [
                    Expanded(
                      child: FloatingSearchBar.builder(
                        padding: EdgeInsets.only(top: 8),
                        itemCount: jotit.notes.length,
                        itemBuilder: (BuildContext context, int i) {
                          if (!jotit.notes[i].title
                                  .toLowerCase()
                                  .contains(_searchQuery) &&
                              !jotit.notes[i].content
                                  .toLowerCase()
                                  .contains(_searchQuery)) {
                            return Container();
                          }
                          final displayTime = i == 0 ||
                              (i < jotit.notes.length - 1 &&
                                  !jotit.notes[i].modified
                                      .sameDay(jotit.notes[i - 1].modified));
                          return Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              if (displayTime)
                                Container(
                                  padding: const EdgeInsets.only(
                                      left: 80, top: 16.0),
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    jotit.notes[i].modified
                                        .getLocalizedDay(context),
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor),
                                  ),
                                ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: ListTile(
                                  leading: _selected.isEmpty
                                      ? IconButton(
                                          icon: Icon(jotit.notes[i].favorite
                                              ? Icons.star
                                              : Icons.star_border_outlined),
                                          onPressed: () => _toggleFavorite(
                                              jotit, jotit.notes[i].id),
                                        )
                                      : Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 12),
                                          child: Icon(_selected
                                                  .contains(jotit.notes[i].id)
                                              ? Icons.check_box_rounded
                                              : Icons
                                                  .check_box_outline_blank_rounded),
                                        ),
                                  selected:
                                      _selected.contains(jotit.notes[i].id),
                                  title:
                                      Text(jotit.notes[i].title, maxLines: 1),
                                  subtitle:
                                      Text(jotit.notes[i].content, maxLines: 3),
                                  onTap: () => _selected.isNotEmpty
                                      ? _toggleSelect(jotit.notes[i].id)
                                      : Navigator.of(context)
                                          .push(
                                            MaterialPageRoute(
                                              builder: (_) => TaskView(
                                                  note: jotit.notes[i]),
                                            ),
                                          )
                                          .then((_) => _sync(jotit)),
                                  onLongPress: () =>
                                      _toggleSelect(jotit.notes[i].id),
                                  trailing: !jotit.cloudMode ||
                                          jotit.notes[i].synchronized
                                      ? null
                                      : Icon(Icons.cloud_upload_outlined),
                                ),
                              ),
                            ],
                          );
                        },
                        trailing: _isLoading
                            ? CircularProgressIndicator()
                            : InkWell(
                                borderRadius: BorderRadius.circular(90),
                                child: CircleAvatar(
                                  child: Text('J'),
                                ),
                                onTap: () => jotit.cloudMode
                                    ? _sync(jotit)
                                    : Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (_) => CloudSettingsView(),
                                        ),
                                      ),
                              ),
                        drawer: Drawer(
                          child: ListView(
                            children: [
                              ListTile(
                                leading: Icon(Icons.settings),
                                title: Text('Cloud Settings'),
                                onTap: () => Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (_) => CloudSettingsView(),
                                  ),
                                ),
                              ),
                              ListTile(
                                leading: Icon(Icons.brightness_2_outlined),
                                title: Text('Dark mode'),
                                onTap: () =>
                                    AdaptiveTheme.of(context).toggleThemeMode(),
                              ),
                              Divider(height: 1),
                              ListTile(
                                leading: Icon(Icons.security_outlined),
                                title: Text('Privacy'),
                                onTap: () => launch(
                                    'https://gitlab.com/ChristianPauly/jotit-cloud-notes/-/blob/main/PRIVACY.md'),
                              ),
                              ListTile(
                                leading: Icon(Icons.source_outlined),
                                title: Text('Source code'),
                                onTap: () => launch(
                                    'https://gitlab.com/ChristianPauly/jotit-cloud-notes'),
                              ),
                              ListTile(
                                leading: Icon(Icons.info_outline),
                                title: Text('About Jotit'),
                                onTap: () => showAboutDialog(
                                    context: context,
                                    applicationName: AppConfig.name),
                              ),
                            ],
                          ),
                        ),
                        onChanged: (String value) =>
                            setState(() => _searchQuery = value.toLowerCase()),
                        onTap: () {},
                        decoration: InputDecoration.collapsed(
                          hintText: 'Search...',
                        ),
                      ),
                    ),
                    AnimatedContainer(
                      height: _selected.isEmpty ? 0 : 56,
                      duration: Duration(milliseconds: 250),
                      child: Material(
                        elevation: 10,
                        child: Row(
                          children: [
                            IconButton(
                              icon: Icon(Icons.close),
                              onPressed: () =>
                                  setState(() => _selected.clear()),
                            ),
                            Text('${_selected.length} ausgewählt'),
                            Spacer(),
                            IconButton(
                              icon: Icon(Icons.delete),
                              onPressed: () => _deleteSelected(jotit),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
