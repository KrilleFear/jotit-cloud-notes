import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/material.dart';
import 'package:jotit_cloud_notes/config/app_config.dart';
import 'package:jotit_cloud_notes/model/jotit_cloud_notes.dart';
import 'package:jotit_cloud_notes/views/task_list_view.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(JotitCloudNotesApp());
}

class JotitCloudNotesApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (_) => JotitCloudNotes(),
      child: AdaptiveTheme(
        light: ThemeData(
          brightness: Brightness.light,
          scaffoldBackgroundColor: Colors.white,
          appBarTheme: AppBarTheme(
            brightness: Brightness.light,
            color: Colors.white,
            textTheme: TextTheme(
              headline6: TextStyle(
                color: Colors.black,
                fontSize: 20,
              ),
            ),
            iconTheme: IconThemeData(color: Colors.black),
          ),
        ),
        dark: ThemeData(
          brightness: Brightness.dark,
          scaffoldBackgroundColor: Colors.black,
          appBarTheme: AppBarTheme(
            brightness: Brightness.dark,
            color: Color(0xff1D1D1D),
            textTheme: TextTheme(
              headline6: TextStyle(
                color: Colors.white,
                fontSize: 20,
              ),
            ),
            iconTheme: IconThemeData(color: Colors.white),
          ),
        ),
        initial: AdaptiveThemeMode.light,
        builder: (theme, darkTheme) => MaterialApp(
          theme: theme,
          darkTheme: darkTheme,
          title: AppConfig.name,
          home: TaskListView(),
        ),
      ),
    );
  }
}
