import 'dart:async';
import 'dart:io';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flushbar/flushbar_helper.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:jotit_cloud_notes/model/jotit_cloud_notes.dart';
import 'package:jotit_cloud_notes/model/note.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';
import '../utils/date_time_extension.dart';

class TaskView extends StatefulWidget {
  final Note note;

  const TaskView({Key key, this.note}) : super(key: key);
  @override
  _TaskViewState createState() => _TaskViewState();
}

class _TaskViewState extends State<TaskView> {
  Note note;
  Timer _coolDown;
  Future<void> _syncFuture;
  bool _editMode;
  JotitCloudNotes jotit;

  void _updateNote(String content, {duration}) {
    duration ??= Duration(seconds: 1);
    _coolDown?.cancel();
    _coolDown = Timer(duration, () {
      if (!mounted) return;
      if (note == null) {
        setState(() {
          _syncFuture =
              jotit.createNote(content).then((n) => setState(() => note = n));
        });
      } else {
        setState(() {
          _syncFuture = jotit.updateNote(note.id, content: content);
        });
      }
    });
  }

  void _setTitle(BuildContext context) async {
    final input = await showTextInputDialog(
      context: context,
      title: 'Set title',
      textFields: [
        DialogTextField(hintText: note.title, initialText: note.title)
      ],
    );
    if (input != null) {
      setState(() {
        _syncFuture = jotit
            .updateNote(note.id, title: input.single)
            .then((n) => setState(() => note = n));
      });
    }
  }

  void _copyAction() {
    if (!kIsWeb && (Platform.isAndroid || Platform.isIOS)) {
      Share.share(note.content, subject: note.title);
    } else {
      FlutterClipboard.copy(note.content).then(
        (_) => FlushbarHelper.createSuccess(message: 'Copied to clipboard'),
      );
    }
  }

  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    note = widget.note;
    _editMode = note == null;
    _controller.text = widget.note?.content ?? '';
  }

  @override
  Widget build(BuildContext context) {
    jotit = JotitCloudNotes.of(context);
    return Scaffold(
      appBar: AppBar(
        title: ListTile(
          contentPadding: EdgeInsets.zero,
          title: Text(
            note?.title ?? 'Neue Notiz',
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          subtitle: Text(
            (note?.modified ?? DateTime.now()).getLocalizedDayTime(context),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          onTap: note == null ? null : () => _setTitle(context),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.share_outlined),
            // TODO: Implement
            onPressed: note == null ? null : _copyAction,
          ),
          IconButton(
            icon: Icon(_editMode ? Icons.edit_off : Icons.edit_outlined),
            onPressed: () => setState(() => _editMode = !_editMode),
          ),
        ],
        elevation: 0,
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(2),
          child: _syncFuture != null
              ? FutureBuilder(
                  future: _syncFuture,
                  builder: (_, snapshot) {
                    if (snapshot.connectionState != ConnectionState.done) {
                      return Center(
                          child: LinearProgressIndicator(minHeight: 2));
                    }

                    return Container();
                  },
                )
              : SizedBox(height: 2),
        ),
      ),
      body: _editMode
          ? TextField(
              expands: true,
              minLines: null,
              maxLines: null,
              autofocus: true,
              controller: _controller,
              onChanged: (String content) => _updateNote(content),
              style: TextStyle(fontSize: 14),
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.all(16.0),
              ),
            )
          : Markdown(
              data: _controller.text,
              selectable: true,
              onTapLink: (a, b, c) => launch(b),
            ),
    );
  }
}
