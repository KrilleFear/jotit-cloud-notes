import 'dart:math';

import 'package:flutter/material.dart';

class Note {
  String id;
  int cloudId;
  DateTime modified;
  String title;
  String category;
  String content;
  bool favorite;
  bool synchronized;

  static int _counter = DateTime.now().millisecondsSinceEpoch;

  Note({
    String id,
    this.cloudId,
    DateTime modified,
    String title,
    this.category,
    @required this.content,
    this.favorite = false,
    this.synchronized = false,
  })  : title = title ??
            content
                .split('\n')
                .first
                .substring(0, min(content.split('\n').first.length, 40)),
        modified = modified ?? DateTime.now(),
        id = id ?? (_counter++).toString();

  Note.fromJson(Map<String, dynamic> json) {
    id = json['local_id'] ?? (_counter++).toString();
    cloudId = json['id'];
    modified = DateTime.fromMillisecondsSinceEpoch(json['modified'] * 1000);
    title = json['title'] ?? '';
    category = json['category'] ?? '';
    content = json['content'];
    favorite = json['favorite'] ?? false;
    synchronized = json['synchronized'] ?? false;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = cloudId;
    data['local_id'] = id;
    data['modified'] = (modified.millisecondsSinceEpoch / 1000).round();
    data['title'] = title;
    data['category'] = category;
    data['content'] = content;
    data['favorite'] = favorite;
    data['synchronized'] = synchronized;
    return data;
  }
}
