import 'package:flutter/material.dart';

class NextcloudCredentials {
  String url;
  String username;
  String password;

  NextcloudCredentials(
      {@required this.url, @required this.username, @required this.password});

  NextcloudCredentials.fromJson(Map<String, dynamic> json) {
    url = json['url'];
    username = json['username'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['url'] = url;
    data['username'] = username;
    data['password'] = password;
    return data;
  }
}
